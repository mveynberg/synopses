* Rails 7 will have three great answers to JavaScript in 2021+
** Intro
*** Full-stack strategy — enduring source of controversy
    Rails unapologetically full stack since the beginning.

    Default answers to all the major questions.

    Key to success.

    What's too much to include? What's not enough?

*** .
    The Rails Doctrine.

    Third pillar of The Menu Is Omakase.

    ...
